﻿using System.ComponentModel.DataAnnotations;

namespace HisserFullStack.Models
{
    public class NewPostVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [StringLength(256, MinimumLength = 3, ErrorMessage = "Title must be between 3 and 256 characters")]
        public string Title { get; set; }
        public DateTime LastUpdatedDateTime { get; set; }
        [Required(ErrorMessage = "Description is required")]
        [StringLength(1000, MinimumLength = 3, ErrorMessage = "Description must be between 3 and 1000 characters")]
        public string Description { get; set; }
    }
}