﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace HisserFullStack.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "Profile Picture URL")] 
        public string ProfilePictureURL { get; set; }
    }
}