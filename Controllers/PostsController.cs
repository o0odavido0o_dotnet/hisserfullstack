﻿using HisserFullStack.Data.Services;
using HisserFullStack.Data.Static;
using HisserFullStack.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace HisserFullStack.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        private readonly IPostsService _service;

        public PostsController(IPostsService service)
        {
            _service = service;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            var allPosts = await _service.GetAllAsync(n => n.User);
            return View(allPosts);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Filter(string searchString)
        {
            var allPosts = await _service.GetAllAsync(n => n.User);

            if (!string.IsNullOrEmpty(searchString))
            {
                var filteredResult = allPosts.Where(n => n.Title.Contains(searchString) || n.Description.Contains(searchString)).ToList();
                return View("Index", filteredResult);
            }

            return View("Index", allPosts);
        }

        //Get: Posts/Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(NewPostVM postVM)
        {
            if (!ModelState.IsValid)
            {
                return View(postVM);
            }

            var post = new Post
            {
                Id = postVM.Id,
                Title = postVM.Title,
                Description = postVM.Description,
                UserId = User.FindFirstValue(ClaimTypes.NameIdentifier)
            };

            await _service.AddAsync(post);
            return RedirectToAction(nameof(Index));
        }

        //Get: Posts/Edit/1
        public async Task<IActionResult> Edit(int id)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var post = await _service.GetPostByIdAsync(id);
            if (post == null || !post.UserId.Equals(userId))
                return View("NotFound");

            var postVM = new NewPostVM
            {
                Id = post.Id,
                Title = post.Title,
                Description = post.Description,
            };

            return View(postVM);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, NewPostVM postVM)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var post = await _service.GetPostByIdAsync(postVM.Id);
            if (id != postVM.Id || !post.UserId.Equals(userId))
            {
                return View("NotFound");
            }

            if (!ModelState.IsValid)
            {
                return View(postVM);
            }

            await _service.UpdatePostAsync(postVM);
            return RedirectToAction(nameof(Index));
        }

        //Get: Posts/Delete/1
        public async Task<IActionResult> Delete(int id)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userRole = User.FindFirstValue(ClaimTypes.Role);
            var post = await _service.GetPostByIdAsync(id);
            if (post == null || (!post.UserId.Equals(userId) && !userRole.Equals(UserRoles.Admin)))
                return View("NotFound");

            return View(post);
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            string userRole = User.FindFirstValue(ClaimTypes.Role);
            var post = await _service.GetPostByIdAsync(id);
            if (post == null || (!post.UserId.Equals(userId) && !userRole.Equals(UserRoles.Admin)))
            {
                return View("NotFound");
            }

            await _service.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }
}