﻿using HisserFullStack.Data.Base;
using HisserFullStack.Models;



namespace HisserFullStack.Data.Services
{
    public interface IPostsService : IEntityBaseRepository<Post>
    {
        Task<Post> GetPostByIdAsync(int id);
        Task UpdatePostAsync(NewPostVM data);
    }
}