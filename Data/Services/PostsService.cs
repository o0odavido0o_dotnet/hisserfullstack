﻿using HisserFullStack.Data.Base;
using HisserFullStack.Models;
using Microsoft.EntityFrameworkCore;

namespace HisserFullStack.Data.Services
{
    public class PostsService : EntityBaseRepository<Post>, IPostsService
    {
        private readonly AppDbContext _context;
        public PostsService(AppDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Post> GetPostByIdAsync(int id)
        {
            var post = await _context.Posts
                .Include(u => u.User)
                .FirstOrDefaultAsync(n => n.Id == id);

            return post;
        }

        public async Task UpdatePostAsync(NewPostVM data)
        {
            var dbPost = await _context.Posts.FirstOrDefaultAsync(n => n.Id == data.Id);

            if (dbPost != null)
            {
                dbPost.Title = data.Title;
                dbPost.Description = data.Description;
                dbPost.LastUpdatedDateTime = data.LastUpdatedDateTime;
                await _context.SaveChangesAsync();
            }
        }
    }
}