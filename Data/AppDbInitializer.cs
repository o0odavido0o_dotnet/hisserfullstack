﻿using HisserFullStack.Data.Static;
using HisserFullStack.Models;
using Microsoft.AspNetCore.Identity;
using System.Linq.Expressions;

namespace HisserFullStack.Data
{
    public class AppDbInitializer
    {
        public static void Seed(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<AppDbContext>();

                context.Database.EnsureCreated();

                //Users
                /*if (!context.Users.Any())
                {
                    context.Users.AddRange(new List<User>()
                    {
                        new User()
                        {
                            UserName = "Grey Warden",
                            ProfilePictureURL = "",
                        }
                    });
                    context.SaveChanges();
                }*/

                //Posts
                if (!context.Posts.Any())
                {
                    context.Posts.AddRange(new List<Post>()
                    {
                        new Post()
                        {
                            Title = "Grey Wardens Motto",
                            Description = @"In war, victory.
In peace, vigilance.
In death, sacrifice.",
                            LastUpdatedDateTime = DateTime.Now,
                            UserId = "9e02bd05-8a63-4b12-af57-5c226e623f54",
                        },
                        new Post()
                        {
                            Title = "Grey Wardens joining speech",
                            Description = @"Join us, brothers and sisters.
Join us in the shadows 
where we stand vigilant.
Join us as we carry the duty
that can not be forsworn.
And should you perish,
know that yout sacrifice
will not be forgotten.
And that one day we shall join you",
                            LastUpdatedDateTime = DateTime.Now,
                            UserId = "9e02bd05-8a63-4b12-af57-5c226e623f54",
                        }
                    });
                    context.SaveChanges();
                }
            }
        }

        public static async Task SeedUsersAndRolesAsync(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.CreateScope())
            {
                //Roles
                var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                if (!await roleManager.RoleExistsAsync(UserRoles.Admin))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
                if (!await roleManager.RoleExistsAsync(UserRoles.User))
                    await roleManager.CreateAsync(new IdentityRole(UserRoles.User));

                //Users
                var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

                var adminUser = await userManager.FindByEmailAsync("admin@hisser.com");
                if(adminUser == null)
                {
                    var newAdminUser = new ApplicationUser
                    {
                        ProfilePictureURL = "",
                        UserName = "admin",
                        Email = "admin@hisser.com",
                        EmailConfirmed = true,
                    };
                    await userManager.CreateAsync(newAdminUser, "FullStack@1234?");
                    await userManager.AddToRoleAsync(newAdminUser, UserRoles.Admin);
                }

                var greyWardenUser = await userManager.FindByEmailAsync("grey.warden@hisser.com");
                if (greyWardenUser == null)
                {
                    var newGreyWardenUser = new ApplicationUser
                    {
                        ProfilePictureURL = "",
                        UserName = "GreyWarden",
                        Email = "grey.warden@hisser.com",
                        EmailConfirmed = true,
                    };
                    await userManager.CreateAsync(newGreyWardenUser, "FullStack@1234?");
                    await userManager.AddToRoleAsync(newGreyWardenUser, UserRoles.User);
                }
            }
        }
    }
}